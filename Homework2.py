#!/usr/bin/env python
"""Oskar Myczka - homework 2"""


class Flor:
    pass


class Cosmetic:
    pass


class Pastrie:
    pass


polish_flora = Flor()
danica_flora = Flor()
european_flora = Flor()

perfume = Cosmetic()
cream = Cosmetic()
deodorant = Cosmetic()

bread = Pastrie()
roll = Pastrie()
cake = Pastrie()

print(polish_flora, danica_flora, european_flora)
print(perfume, cream, deodorant)
print(bread, roll, cake)
